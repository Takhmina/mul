from tkinter import *
import random
import Test
from threading import *
import Sounds
import pygame
import time
import io
from PIL import Image
import datetime

# global var
WIDTH = 1200
HEIGHT = 800
# pads
PAD_W = 300
PAD_H = 50
PAD_SPEED = 15
TOP_PAD_SPEED = 0
BOT_PAD_SPEED = 0

# bal
BALL_RADIUS = 20
BALL_SPEED_UP = 1.05
BALL_MAX_SPEED = 40
BALL_X_SPEED = 20
BALL_Y_SPEED = 20
INITIAL_SPEED = 20
BALL_X_CHANGE = 0
BALL_Y_CHANGE = 20

# bottom line coords
bottom_line_distance = HEIGHT - 50

# players score
PLAYER_1_SCORE = 0
PLAYER_2_SCORE = 0
WIN_SCORE = ''

# Variables for coordinates and speed
pos = 0
avspeed = 0
arrx = []
arry = []

# Timer for Play Time
sek1 = datetime.datetime.now()

# Function to get coordinates from camera
def update(coords, av_speed, arx, ary):
    global pos, avspeed, arrx, arry
    arrx = arx
    arry = ary
    pos = coords * 12
    avspeed = av_speed

# Thread for sound
sound = Thread(target=Sounds.bg_music_play(), args=())
sound.start()

# Thread for web camera
eg = Thread(target=Test.web_vid, args=([update]))
eg.start()

# main window
root = Tk()
root.title("Cat-Pong")

# canvas
c = Canvas(root, width=WIDTH, height=HEIGHT, background="#ff7373")
c.pack()

# Background Image
tk_img = PhotoImage(file='bg.gif')
c.create_image(600, 400, image=tk_img)

# playground elements
c.create_line(0, 50, WIDTH, 50, width=3,fill="white")
c.create_line(0, HEIGHT - 50, WIDTH, HEIGHT - 50, width=3, fill="white")
#c.create_line(0, HEIGHT/2, WIDTH, HEIGHT/2,width=3, fill="white")

p_1_text = c.create_text(100, HEIGHT/2 - 60,
                         text=PLAYER_1_SCORE,
                         font="Arial 20",
                         fill="brown")
p_2_text = c.create_text(100, HEIGHT/2 + 160,
                         text=PLAYER_2_SCORE,
                         font="Arial 20",
                         fill="brown")

BALL = c.create_oval(WIDTH / 2 - BALL_RADIUS / 2,
                     HEIGHT / 2 - BALL_RADIUS / 2,
                     WIDTH / 2 + BALL_RADIUS / 2,
                     HEIGHT / 2 + BALL_RADIUS / 2, fill="white")

TOP_PAD = c.create_line(0, PAD_H/2, PAD_W, PAD_H/2, width=PAD_H, fill="#800000")
BOT_PAD = c.create_line(0, HEIGHT-PAD_H/2, PAD_W, HEIGHT-PAD_H/2, width=PAD_H, fill="#001a66")

# scpre function
def update_score(player):
    global PLAYER_1_SCORE, PLAYER_2_SCORE
    if player == "bot":
        PLAYER_2_SCORE += 1
        c.itemconfig(p_2_text, text=PLAYER_2_SCORE)
    else:
        PLAYER_1_SCORE += 1
        c.itemconfig(p_1_text, text=PLAYER_1_SCORE)

#bounce function
def bounce(action):
    global BALL_Y_SPEED, BALL_X_SPEED
    soundef = pygame.mixer
    soundef.init()
    soundef.music.load("pop.mp3")
    if action == "strike":
        BALL_X_SPEED = random.randrange(-10, 10)
        if abs(BALL_Y_SPEED) < BALL_MAX_SPEED:
            BALL_Y_SPEED *= -BALL_SPEED_UP
            soundef.music.play()
        else:
            BALL_Y_SPEED = -BALL_Y_SPEED
            soundef.music.play()
    else:
        BALL_X_SPEED = -BALL_X_SPEED
        soundef.music.play()


# ball moving
def move_ball():
    global TOP_PAD_SPEED
    ball_left, ball_top, ball_right, ball_bot = c.coords(BALL)
    ball_center = (ball_right + ball_left) / 2
    # move
    if ball_bot + BALL_Y_SPEED < HEIGHT - 50 and \
            ball_top + BALL_Y_SPEED > 50:
        c.move(BALL, BALL_X_SPEED, BALL_Y_SPEED)
    # if ball at the top or bottom
    elif ball_bot == HEIGHT - 50 or ball_top == 50:
        # bottom
        if ball_bot > HEIGHT / 2:
            #if if in pad
            #botoom pad
            if c.coords(BOT_PAD)[0] < ball_center < c.coords(BOT_PAD)[2]:
                if c.coords(TOP_PAD)[0] + PAD_W / 2 < ball_center:
                    TOP_PAD_SPEED = PAD_SPEED
                else:
                    TOP_PAD_SPEED = -PAD_SPEED
                bounce("strike")

            else:
                if c.coords(TOP_PAD)[0] + PAD_W / 2 < ball_center:
                    TOP_PAD_SPEED = PAD_SPEED
                else:
                    TOP_PAD_SPEED = -PAD_SPEED
                update_score("top")
                spawn_ball()
        #top pad
        else:
            if c.coords(TOP_PAD)[0] < ball_center < c.coords(TOP_PAD)[2]:
                if c.coords(TOP_PAD)[0] + PAD_W / 2 < ball_center:
                    TOP_PAD_SPEED = PAD_SPEED
                else:
                    TOP_PAD_SPEED = -PAD_SPEED
                bounce("strike")
            else:
                if c.coords(TOP_PAD)[0] + PAD_W / 2 < ball_center:
                    TOP_PAD_SPEED = PAD_SPEED + 10
                else:
                    TOP_PAD_SPEED = -PAD_SPEED
                update_score("bot")
                spawn_ball()
    else:
        if ball_bot > HEIGHT / 2:
            if c.coords(TOP_PAD)[0] + PAD_W / 2 < ball_center:
                TOP_PAD_SPEED = PAD_SPEED
            else:
                TOP_PAD_SPEED = -PAD_SPEED
            c.move(BALL, BALL_X_SPEED, bottom_line_distance - ball_bot)
        else:
            if c.coords(TOP_PAD)[0] + PAD_W / 2 < ball_center:
                TOP_PAD_SPEED = PAD_SPEED
            else:
                TOP_PAD_SPEED = -PAD_SPEED
            c.move(BALL, BALL_X_SPEED, -ball_top + PAD_H )
    # ricoshet from leaft or right
    if ball_left + BALL_X_SPEED < 0 or ball_right  + BALL_X_SPEED > WIDTH:
        if c.coords(TOP_PAD)[0] + PAD_W / 2 < ball_center:
            TOP_PAD_SPEED = PAD_SPEED
        else:
            TOP_PAD_SPEED = -PAD_SPEED
        bounce("ricochet")

# pads move
def move_pads():
    PADS = {TOP_PAD: TOP_PAD_SPEED,
            }
    for pad in PADS:
        c.move(pad, PADS[pad], 0)
        if c.coords(pad)[0] < 0:
            c.move(pad, -c.coords(pad)[0] , 0)
        elif c.coords(pad)[2]> WIDTH:
            c.move(pad, -c.coords(pad)[0] + WIDTH - PAD_W, 0)

# Player move
def move_bot_pad():
    global pos, BOT_PAD
    c.delete(BOT_PAD)
    BOT_PAD = c.create_line(pos-150, HEIGHT-PAD_H/2, pos+150, HEIGHT-PAD_H/2, width=PAD_H, fill="#2e5cb8")
    c.update()

# Spawn the ball
def spawn_ball():
    global BALL_Y_SPEED
    c.coords(BALL, WIDTH/2-BALL_RADIUS/2,
             HEIGHT/2-BALL_RADIUS/2,
             WIDTH/2+BALL_RADIUS/2,
             HEIGHT/2+BALL_RADIUS/2)
    c.update()
    BALL_Y_SPEED = -(BALL_Y_SPEED * -INITIAL_SPEED) / abs(BALL_Y_SPEED)


#Main function
def main():
    global PLAYER_1_SCORE, PLAYER_2_SCORE, WIN_SCORE, sek1
    if PLAYER_1_SCORE > 14 or PLAYER_2_SCORE > 14:
        sek2 = datetime.datetime.now()
        playtime = (int(sek2.strftime("%M"))- int(sek1.strftime("%M")))*60 + int(sek2.strftime("%S")) - int(sek1.strftime("%S"))
        if PLAYER_2_SCORE > PLAYER_1_SCORE:
            WIN_SCORE = "You WIN! Play time: " + str(playtime) + " sec"
            soundef = pygame.mixer
            soundef.init()
            soundef.music.load("yay.mp3")
            soundef.music.play()
        if PLAYER_2_SCORE == PLAYER_1_SCORE:
            WIN_SCORE = "TIE. Play time: " + str(playtime) + " sec"
            soundef = pygame.mixer
            soundef.init()
            soundef.music.load("loser.mp3")
            soundef.music.play()
        if PLAYER_2_SCORE < PLAYER_1_SCORE:
            WIN_SCORE = "You LOSE! Play time: " + str(playtime) + " sec"
            soundef = pygame.mixer
            soundef.init()
            soundef.music.load("loser.mp3")
            soundef.music.play()
        root.destroy()
    else:
        move_ball()
        move_pads()
        move_bot_pad()
        root.after(30, main)

main()
root.mainloop()

# Score window
root1 = Tk()
root1.title("Score")
positionRight = int(root1.winfo_screenwidth() / 2 - 300)
positionDown = int(root1.winfo_screenheight() / 2 - 300)
root1.geometry("+{}+{}".format(positionRight, positionDown))

# canvas
c1 = Canvas(root1, width=600, height=600, background="#ff7373")
c1.pack()

# Background for score window
tk_img1 = PhotoImage(file='score.gif')
c1.create_image(300, 300, image=tk_img1)

# Score text
score_text = c1.create_text(300, 200,
                         text = WIN_SCORE,
                         font="Arial 20",
                         fill="brown")
# Speed text
speed_text = c1.create_text(300, 250,
                         text = "Your average speed was: " + str(round(avspeed)) + " cm/s",
                         font="Arial 20",
                         fill="brown")


def get_color(x):
    col_ind = ""
    if x < 5 :
        col_ind = "#4d0000"
    elif x < 10 and x >= 5:
        col_ind = "#990000"
    else:
        col_ind = "#ff4d4d"
    return col_ind

# Draw heatmap ---------
def show_heatmap():
    global arry, arrx
    root3 = Tk()
    root3.title("Score")
    positionRight = int(root3.winfo_screenwidth() / 2 - 400)
    positionDown = int(root3.winfo_screenheight() / 2 - 200)
    root3.geometry("+{}+{}".format(positionRight, positionDown))

    for r in range(20):
        for c in range(31):

            titlelabel = Label(root3, text="", width=3, fg="blue4", bg="black")
            titlelabel.grid(row=r, column=c, sticky='ew')  # sticky='ew' expands the label horizontally
    # canvas
    b = 0
    for i in range(len(arry)):
        col_n = 1
        for j in range(len(arry)):
            if arrx[i] == arrx[j] and arry[i] ==arry[j]:
                col_n = col_n + 1
        col = get_color(col_n)
        titlelabel = Label(root3, text="", width=3, fg="blue4", bg=col )
        titlelabel.grid(row=arry[i], column=arrx[i], sticky='ew')

    root3.mainloop()
# ----------------------


# Save image in directory, name in datetime format
def save_image():
    nowdate = datetime.datetime.now()
    name = str(nowdate.strftime("%Y-%m-%d-%H-%M-%S"))
    ps = c1.postscript(colormode='color')
    img = Image.open(io.BytesIO(ps.encode('utf-8')))
    img.save('results/' + name +'.jpg')

# Button to save score as image
button1 = Button(root1, text = "Save my result!", command = save_image)
button1.configure(width = 13, activebackground = "#ffcc99",font="Arial 20", background="#ffe6cc", foreground="brown" )
button1_window = c1.create_window(195, 300, anchor=NW, window=button1)

button2 = Button(root1, text = "Show heatmap", command = show_heatmap)
button2.configure(width = 13, activebackground = "#ffcc99",font="Arial 20", background="#ffe6cc", foreground="brown" )
button2_window = c1.create_window(195, 360, anchor=NW, window=button2)

# Main of score window
def main1():
    root1.after(30, main1)

main1()
root1.mainloop()

# Stop playing that ANOYING music! Really, i was listening that music a lot
sound = Thread(target=Sounds.bg_stop(), args=())
sound.start()

