import cv2
import numpy as np
from threading import *
import time


# Capture the input frame from webcam
def get_frame(cap):
    # Capture the frame from video capture object
    ret, frame = cap.read()

    dim = (1200, 800)
    # Resize the input frame
    frame = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
    return frame

pos1 = 0
pos2 = 0
width = 0
speed = 0
posx = 0
av_speed = 0
i = 2
coorx = 0
coory = 0
arrx = []
arry = []
def speed():
    global pos1, pos2, width, speed, av_speed, i
    while True:

        pos1 = posx
        time.sleep(0.1)
        pos2 = posx
        if width != 0:
            speed = ((pos2 - pos1) / width) * 7 / 0.1
            if speed < 0:
                speed = speed * (-1)
            av_speed = av_speed + speed/i
        i = i + 1

new_t = Thread(target=speed, args=())
new_t.start()

def pos_arr():
    global coorx, coory, arrx, arry
    time.sleep(2)
    while True:
        x = (coorx * 12) // 40
        y = (coory*8) // 40
        arrx.append(int(x))
        arry.append(int(y))
        time.sleep(0.1)

arr_t = Thread(target=pos_arr, args=())
arr_t.start()

def web_vid(calb):
    global  posx, width, av_speed, coory, coorx, arry, arrx
    cap = cv2.VideoCapture(0)
    coorx = 0;
    max_x = 1100;
    # Iterate until the user presses ESC key
    while True:
        frame = get_frame(cap)

        # Convert the HSV colorspace
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # Define 'blue' range in HSV colorspace
        lower = np.array([100, 150, 0])
        upper = np.array([140, 255, 255])

        # Threshold the HSV image to get only blue color
        mask = cv2.inRange(hsv, lower, upper)

        # Bitwise-AND mask and original image
        res = cv2.bitwise_and(frame, frame, mask=mask)
        res = cv2.medianBlur(res, 5)

        conts, h = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        cv2.drawContours(res, conts, -1, (255, 0, 0), 3)

        max_a = 0

        for i in range(len(conts)):
            x, y, w, h = cv2.boundingRect(conts[i])
            area = w * h
            if area > max_a:
                max_a = area

        for i in range(len(conts)):
            x, y, w, h = cv2.boundingRect(conts[i])
            area1 = w * h
            if area1 == max_a:
                font = cv2.FONT_HERSHEY_SIMPLEX
                cv2.rectangle(res, (x, y), (x + w, y + h), (0, 0, 255), 2)
                #Sending position in percent
                coorx = 100 - ((x * 100) / 1200)
                coory = (y*100)/ 800
                posx = x
                width = w
                cv2.putText(res, str(coorx), (30, 100), font, 4, (255, 255, 255), 2, cv2.LINE_AA)
        calb(coorx, av_speed, arrx, arry)
        c = cv2.waitKey(5)
        cv2.destroyAllWindows()

def speed(pos, width1):
    global posx, width
    pos = posx
    width1 = width


