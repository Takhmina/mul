import pygame

bg_music = pygame.mixer
def bg_music_play():
    bg_music.init()
    bg_music.Channel(0).play(bg_music.Sound('bg.ogg'))

def bg_stop():
    bg_music.stop()